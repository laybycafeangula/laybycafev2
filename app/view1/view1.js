'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$http','$scope', '$location','$cookies',function($http,$scope,$location,$cookies) {

    $scope.username = null;
    $scope.pword = null;


    $scope.postdata = function (username, pword) {
        var data = {
            email: username,
            password: pword,


        };



        //Call the services
        $http.post('https://laybycafeapi.co.za/sandbox/v2.0/login', data)
            .then(function (response) {
                if (response.data) {
                    $scope.msg = "Post Data Submitted Successfully!";


                    var userID = response.data.user_id;
                    var role = response.data.role;

                    // console.log( userID,role);

                    // Put cookie
                    $cookies.put('UserSession', userID);
                    $cookies.put('UserRole', role);
                    // Get cookie
                    var userSessionID = $cookies.get('UserSession');
                    var userSessionRole = $cookies.get('UserRole');
                    console.log(userSessionID);
                    console.log(userSessionRole);
/*
                    swal({
                        title: "Good job!",
                        text: "You clicked the button!",
                        icon: "success",



                    });*/
                    $location.url('/profile');

                }

                else if(response.data === null){
                    swal({
                        title: "Warning",
                        text: "Please enter credentialsn!",
                        icon: "warning",



                    });

                }




            }, function (response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response;
                $scope.statustext = response;
                $scope.headers = response.headers();
            });
    }

}])


