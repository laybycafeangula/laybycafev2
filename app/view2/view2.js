'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$http','$scope',function($http,$scope) {

    $scope.firstname = null;
    $scope.lastname = null;
    $scope.email_address = null;
    $scope.idno = null;
    $scope.cellnumber = null;
    $scope.password = null;

    $scope.postdata = function (firstname, lastname,email_address,idno,cellnumber,pass) {
        var data = {
            first_name: firstname,
            last_name: lastname,
            email: email_address,
            cell: cellnumber,
            id_no: idno,
            password: pass


        };

        //Call the services
        $http.post('https://laybycafeapi.co.za/sandbox/v2.0/users', data)
            .then(function (response) {
            if (response.data)
                $scope.msg = "Post Data Submitted Successfully!";

                console.log(response.data)

        }, function (response) {
            $scope.msg = "Service not Exists";
            $scope.statusval = response;
            $scope.statustext = response;
            $scope.headers = response.headers();
        });
    }

}]);