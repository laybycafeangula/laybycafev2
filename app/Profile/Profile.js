'use strict';

angular.module('myApp.profile', ['ngRoute', 'ngCookies'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/profile', {
            templateUrl: 'Profile/Profile.html',
            controller: 'profile1Ctrl'
        });
    }])


    .controller('profile1Ctrl', ['$http','$scope','$cookies',function($http,$scope,$cookies) {

        var userSessionID = $cookies.get('UserSession');
       /* $scope.userIdGet = userSessionID;*/
        $scope.user_address = null;
        $scope.phone_number = null;

        $scope.updateData = function (phone_number,user_address) {

            // Get cookie
            var data = {
                consumer_id: userSessionID,
                address:user_address,
                phone:phone_number
            };
            console.log(data);
            //Call the services
            $http.post('https://laybycafeapi.co.za/sandbox/v2.0/address', data)
                .then(function (response) {
                    if (response.data)
                        $scope.msg = "Post Data Submitted Successfully!";

                    console.log(response.data)

                })
            };



            var userSessionRole = $cookies.get('UserRole');

            var data = {
                user_id: userSessionID,
                role: userSessionRole
            };
             console.log(data);


            $http.get('https://laybycafeapi.co.za/sandbox/v2.0/profile?user_id='+userSessionID+'&role='+userSessionRole+'')
                .then(function (response) {
                    $scope.getProfile= response.data;
                    console.log( $scope.getProfile);
                   // console.log(userSessionRole);

                })

    }]
    )


// $http.get('https://laybycafeapi.co.za/sandbox/v2.0/profile?user_id='+userSessionID+'&role='+userSessionRole+'')